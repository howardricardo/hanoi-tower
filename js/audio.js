//Audio de capturar a peça 
let ClickCapture = new Audio()
ClickCapture.src = 'js/Audios/capture.mp3'

function AudioCapture(){
    ClickCapture.play()
}

// Audio de soltar a peça 
let ClickDrop = new Audio()
ClickDrop.src = 'js/Audios/drop.mp3'

function AudioDrop(){
    ClickDrop.play()
}

//Audio de vitória
let Victorymusic = new Audio()
Victorymusic.src = 'js/Audios/victory.wav'

function AudioVictory(){
    Victorymusic.play()
}