// Cria divs. Se quiser adicionar no body, coloca " id = 'body' " lá no body, no html
function criarDiv(id_da_Div, ondeAdicionar) {
    const Div = document.createElement('div')
    Div.id = id_da_Div
    document.getElementById(ondeAdicionar).appendChild(Div)
}

// Aqui monta a estrutura básica, é só estilizar

criarDiv('bloco', 'body')

function criarPeças() {
    criarDiv('box_start','bloco')
    criarDiv('box_offset','bloco')
    criarDiv('box_end','bloco')
    criarDiv('start', 'box_start')
    criarDiv('offset', 'box_offset')
    criarDiv('end', 'box_end')

    for (let i = 1; i <= 4; i++) {
        criarDiv(`peça${i}`, 'start')
    }
}

function restartGame() {
    Count = 0
    const restart = document.createElement('button')
    restart.className = 'restart'
    const restart_text = document.createTextNode('Jogar de novo')
    restart.appendChild(restart_text)
    restart.onclick = function () {
        let pontuation = document.getElementById('pontuation')
        pontuation.parentNode.removeChild(pontuation)

        let sombra = document.getElementById('sombra')
        sombra.parentNode.removeChild(sombra)

        removeClass('display','contador')
        removeClass('display','reiniciar')
        removeClass('column','bloco')

        document.getElementById('contador').innerHTML = 'Contador de movimentos: 0'
        StartGame()
        Capture(Count)
        AudioCapture()
    }
    document.getElementById('bloco').appendChild(restart)
}

function removeClass(classe,id){
    let div = document.getElementById(id)
    div.classList.remove(classe)
}

// Zerar div bloco e criar peças
function StartGame() {
    const bloco = document.getElementById('bloco')
    bloco.innerHTML = ''
    criarPeças()
}

const titulo = document.createElement('h1')
const titulo_in = document.createTextNode('Torre de Hanoi')
titulo.appendChild(titulo_in)
document.getElementById('bloco').appendChild(titulo)

let Count = 0

const button = document.createElement('button')
const button_text = document.createTextNode('Start')
button.id = 'button_start'
button.appendChild(button_text)
document.getElementById('bloco').appendChild(button)
button.onclick = function () {
    StartGame()
    Capture(Count)
    AudioCapture()

    criarDiv('contador', 'body')

    const div_in = document.createTextNode('Contador de movimentos: 0')
    document.getElementById('contador').appendChild(div_in)

    Count = 0
    const restart = document.createElement('button')
    restart.className = 'restart'
    restart.id = 'reiniciar'
    const restart_text = document.createTextNode('Reiniciar')
    restart.appendChild(restart_text)
    document.body.appendChild(restart)
    restart.onclick = function () {
        let pontuation = document.getElementById('pontuation')
        
        if (pontuation === null) {
            document.getElementById('contador').innerHTML = 'Contador de movimentos: 0'
            StartGame()
            Capture(Count)
            AudioCapture()
        } else {
            let pontuation = document.getElementById('pontuation')
            pontuation.parentNode.removeChild(pontuation)
            document.getElementById('contador').innerHTML = 'Contador de movimentos: 0'
            StartGame()
            Capture(Count)
            AudioCapture()
        }

    }
}