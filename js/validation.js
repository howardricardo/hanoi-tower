function error_MSG() {
    criarDiv('mensagem', 'body')
    let p = document.createElement('p')
    let p_text = document.createTextNode('Você não pode colocar uma peça em cima de uma peça menor!')
    p.appendChild(p_text)
    document.getElementById('mensagem').appendChild(p)
}

function remove_error_MSG() {
    let mensagem = document.getElementById('mensagem')
    mensagem.parentNode.removeChild(mensagem)
}

function validação(tower, piece) {
    if (tower.lastChild === null) {
        AudioDrop()
        Div.appendChild(piece)
        peçaHighlite.classList.remove('highlight')
    }
    if (piece.clientWidth > tower.lastChild.clientWidth) {
        setTimeout(error_MSG)
        setTimeout(remove_error_MSG, 1500)
    } else {
        AudioDrop()
        Div.appendChild(piece)
        peçaHighlite.classList.remove('highlight')
    }
}

function venci(Count) {
    const end = document.getElementById('end')

    if (end.childElementCount === 4) {
        document.getElementById('bloco').innerHTML = ''

        addClass('display','contador')
        addClass('display','reiniciar')
        addClass('column','bloco')

        AudioVictory()
        criarDiv('sombra', 'body')
        criarDiv('pontuation', 'bloco')
        criarDiv('estrelas', 'pontuation')

        if (Count <= 15) {
            restartGame()
            CriarEstrela(3)
        } else if (Count <= 18) {
            restartGame()
            CriarEstrela(2)


        } else if (Count <= 21) {
            restartGame()
            CriarEstrela(1)

        } else {

            restartGame()

            CriarEstrela(0)
        }

    }

    function CriarEstrela(qnt) {
        for (let i = 1; i <= qnt; i++) {
            let star = document.createElement('img')
            star.src = 'js/estrela/estrela.png'
            document.getElementById('estrelas').appendChild(star)

        }
        let p = document.createElement('p')
        let p_text = document.createTextNode(`Você resolveu o puzzle em ${Count} movimentos e conseguiu ${qnt} estrelas!`)
        p.appendChild(p_text)
        document.getElementById('pontuation').appendChild(p)
    }
}

function addClass(classe,id){
    let div = document.getElementById(id)
    div.classList.add(classe)
}