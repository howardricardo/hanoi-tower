// Capturar peças
function Capture(Count) {
    const Peças = document.querySelectorAll('#bloco > div')
    let ClickPeça
    let ClickDiv
    
    Peças.forEach(element => {
        element.addEventListener('click', handdleClick)
    });

    function handdleClick(event) {
        //Verifica se tem algo dentro da variavel ClickPeça
        if (ClickPeça == undefined) {
            AudioCapture()
            ClickPeça = event.currentTarget.lastChild.lastChild
            peçaHighlite = document.getElementById(ClickPeça.id)
            peçaHighlite.classList.add('highlight')
        } else {
            // se tiver algo na variavel, pendura na proxima div clicada.
            ClickDiv = event.currentTarget.lastChild
            Div = document.getElementById(ClickDiv.id)
            Count += 1
            let contador = document.getElementById('contador')
            contador.innerHTML = `Contador de movimentos: ${Count}`
            validação(ClickDiv, ClickPeça)
            venci(Count)
            ClickPeça = undefined
        }
    }
}